import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Layout'),
        debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({required this.title});

  final String title;


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final mycontroller_user = TextEditingController();
  final mycontroller_password = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          constraints: BoxConstraints.expand(),
          color: Colors.lightBlueAccent[100],
          child: SingleChildScrollView(
          child: Column(
            children: [
              Icon(
                //Icons.insert_photo,
                Icons.account_circle_rounded,
                size: 200,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "Row Child 1",
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    "Row Child 2",
                    style: TextStyle(fontSize: 18),
                  ),
                  Text(
                    "Row Child 3",
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Text(
                  "This is column.",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: mycontroller_user,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'User name',
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: mycontroller_password,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Password',
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: (){
                        mycontroller_user.clear();
                        mycontroller_password.clear();
                      },
                      child: Text("Cancel"),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blue, //background color
                      onPrimary: Colors.white, // foreground color-text
                      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(
                        msg: "Username: " +
                            mycontroller_user.text +
                            "\n" +
                            "Password: " +
                            mycontroller_password.text,
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.TOP,
                        timeInSecForIosWeb: 3,
                        textColor: Colors.black45,
                        fontSize: 18.0,
                      );
                      print("Username: " + mycontroller_user.text);
                      print("Password: " + mycontroller_password.text);
                    },
                    child: Text("Login"),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.lightBlueAccent,
                      onPrimary: Colors.white,
                      padding:
                      EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                    ),
                  ),

                ],
              ),
            ],
          ),
          ),
        ));
  }


}

